#!/usr/bin/env bash
rm -rf .coverage
coverage run test/test_binary_search.py
coverage report
coverage xml
