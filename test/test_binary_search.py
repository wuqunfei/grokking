import sys
import os

sys.path.append(os.getcwd())

from unittest import TestCase
from source.binary_search import binary_search


class TestBinarySearch(TestCase):
    list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    item = 6

    def test_binary_search(self):
        index = binary_search(self.list, self.item)
        self.assertEqual(index, 5)

    def test_not_found(self):
        index = binary_search(self.list, 11)
        self.assertIsNone(index)
