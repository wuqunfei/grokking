import sys
import os
from unittest import TestCase

sys.path.append(os.getcwd())
from source.select_sort import SelectSort



class TestSelectSort(TestCase):
    def test_sort(self):
        in_data = [2, 1, 3, 9, 3, 10]
        out_data = SelectSort(in_data).sort()
        self.assertEqual(out_data, sorted(in_data))
