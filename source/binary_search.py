def binary_search(collection, item):
    low = 0
    high = len(collection) - 1
    while low < high:
        mid = (low + high) // 2
        guess = collection[mid]
        if guess is item:
            return mid
        if guess > item:
            high = mid - 1
        else:
            low = mid + 1
    return None
