import copy


class SelectSort:

    def __init__(self, arr):
        self.in_array = copy.deepcopy(arr)
        self.out_array = []

    def find_smallest(self):
        smallest_index = 0
        smallest = self.in_array[smallest_index]
        for i in range(1, len(self.in_array)):
            if self.in_array[i] < smallest:
                smallest = self.in_array[i]
                smallest_index = i
        return self.in_array.pop(smallest_index)

    def sort(self):
        for i in range(len(self.in_array)):
            smallest = self.find_smallest()
            self.out_array.append(smallest)
        return self.out_array

